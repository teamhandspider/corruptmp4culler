﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace corruptMp4Culler
{
    class Program
    {
        private static string path;

        static void Main(string[] args) {            
            if(args.Length > 0) {
                path = args[0];
            }
            else {
                path = "corrtest";
            }            
            

            var dir = new DirectoryInfo(path);
            if(!dir.Exists) {
                ProcessFile(new FileInfo(path));
                return;
            }

            var mps4s = dir.GetFiles("*.mp4");

            int cnt = 0;
            foreach (var item in mps4s) {
                cnt++;
                Console.WriteLine(cnt + " / " + mps4s.Length);
                ProcessFile(item);
            }

            Console.WriteLine("Done");
            Console.ReadKey();
        }

        private static void ProcessFile(FileInfo item) {
            Console.WriteLine("Checking "+item.Name);
            var output = ExecCommand("ffprobe " + item.FullName);
            Console.WriteLine("FFPROBE OUTPUT:" + output);

            //var isBad = output.Contains("Invalid data found when processing input");
            var isBad = !output.Contains("Metadata:");

            if (isBad) {
                Console.WriteLine("\t" + item.Name + " is corrupt, moving to BadFiles");
                var thisLevelBadFilesDir = new DirectoryInfo(Path.Combine(item.Directory.FullName, "BadFiles"));
                thisLevelBadFilesDir.Create();
                item.MoveTo(Path.Combine(thisLevelBadFilesDir.FullName, item.Name));
                //Console.ReadKey();

                AppendToErrors(item.FullName + " is bad, ffprobe output:" + output + "\n\n");
            }
            else {
                Console.WriteLine("\t"+item.Name + " is fine");
            }
            Console.WriteLine();
        }

        private static void AppendToErrors(string v) {
            File.AppendAllText(Path.Combine(path, "corruptMp4CullerErrors.txt"), System.DateTime.Now + ":" + v);
        }

        private static string ExecCommand(string v) {
            Console.WriteLine("ExecCommand " + v);
            var isWindows = System.Environment.MachineName == "MAX-PC";

            Process proc = new Process();
            if(isWindows) {
                proc.StartInfo.FileName = "cmd.exe";
                proc.StartInfo.Arguments = "/C \"" + v + "\"";
            }
            else {
                proc.StartInfo.FileName = "/bin/bash";
                proc.StartInfo.Arguments = "-c \""+ v +"\"";
            }
            proc.StartInfo.UseShellExecute = false;
            //proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.Start();

            proc.WaitForExit();

            var output = proc.StandardError.ReadToEnd();
            return output;
        }
    }
}
